-- This mod add a function to add written books to the treasurer
-- 

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local worldpath = minetest.get_worldpath()

-- Load support for intllib.
local S, NS
if intllib and intllib.make_gettext_pair then
S, NS = dofile(modpath.."/intllib.lua")
else S, NS = function(s) return s, s end
end

local max_title_size = 80
local short_title_size = 35
local lpp = 14 --14 -- Lines per book's page
local no_title = "Untitled Book"
local no_owner = minetest.formspec_escape("...")


local line_parse = function(line)
	local op = ","
	local pt = {
		{"^# ","#FF9292,"},
		{"^## ","#9292DB,"},
		{"^### ","#92FF92,"},
		{"^#### ","#FFFF00,"},
	}
	for _,v in ipairs(pt) do
		if string.match(line,v[1]) then
			line = string.gsub(line,v[1],"")
			op = v[2]
			break
		end
	end
	line = op .. minetest.formspec_escape(line)
	return line
end

-- --[[ Alternate scrollable book unsing table
written_books.register_book_alernate.scroll = function(dt, prob, pre, booktype)
	if not ( dt and dt.title and dt.filetable ) then return end
	local text
	-- Get a stack of one book
	local bookstack = ItemStack(modname..":scroll")
	-- Open stack meta
	local meta = bookstack:get_meta()
	local text = dt.filetable
	
	-- Set book meta
	local data = {}
--	local data = meta:to_table().fields
	local title = dt.title:sub(1, max_title_size)
	local text = text or {}
	for n,l in ipairs(text) do text[n] = line_parse(l) end
	local author = dt.owner
	 
	local short_title = dt.title
	-- Don't bother triming the title if the trailing dots would make it longer
	if #short_title > short_title_size + 3 then
		short_title = short_title:sub(1, short_title_size) .. "..."
	end
	local crd = ""
	if author ~= no_owner then crd =  "by "..author end
	data.description = "\""..short_title.."\""..crd

	local deco = default.gui_bg .. default.gui_bg_img
	local formspec = "size[7,7,true]" .. deco
	-- Title
	formspec = formspec	.. "tablecolumns[color;text]" 
		.. "tableoptions[background=#00000000;highlight=#00000000;border=false]" 
		.. "table[0.4,0;7,0.5;title;#FFFF00," .. minetest.formspec_escape(title) .. "]"
		..	"label[0.5,0.5;by " .. author .. "]"

	-- Content
	formspec = formspec .. "tablecolumns[color;text,align=inline]" --,width=5 
			.. "tableoptions[color=#FFF;background=#494949;highlight=#494949;border=true]" 
			.. "table[0,1;6.7,6;doc;" .. table.concat(text, ",") .. ",] "

--	formspec = formspec .. "image_button_exit[6,6.5;0.8,0.8;power.png;close;;false;false;power.png]"
	
	
	data.owner = author
	data.formspec = formspec
	meta:from_table({ fields = data })

	-- Convert back to itemstack (string) 
	local wbook = bookstack:to_string()

	-- Add to list
	written_books.add_book_to_list(wbook, prob, pre, booktype)
end
--]]

local function book_on_use(itemstack, user)
	local player_name = user:get_player_name()
	--if not user then return end
	--local username = user:get_player_name()
	if not player_name or player_name == "" then return end
	local meta = itemstack:get_meta()
	local data = meta:to_table().fields
	print(dump(data))
	if data.formspec then minetest.show_formspec(player_name, modname..":book", data.formspec) end
	return itemstack
end

minetest.register_craftitem(modname..":scroll", {
	description = S("Scroll"),
	inventory_image = "written_books_scroll.png",
	groups = {book = 1, not_in_creative_inventory = 1, flammable = 3},
	stack_max = 1,
	on_use = book_on_use,
})
